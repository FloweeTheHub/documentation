+++
date = "2018-01-01T07:00:00+01:00"
title = "Flowee the Hub"
draft = false
weight = 10
description = "Flowee the Hub boring manual"
bref = "Hub platform reference manual"
toc = false
singlepage = false
+++

Welcome to the Hub reference manual.

### Overview

Flowee is a group of products which are all made to work well together and
provide you the shortest way to Bitcoin&nbsp;(BCH).

At the center of Flowee is the Hub. The Hub connects to the Bitcoin network
and does tasks like downloading transactions, downloading blocks and
validating everything. The Hub has APIs which many in the Flowee ecosystem
use to access the Bitcoin data.

It is useful to look at the hub like it is a Bitcoin oracle, it has the
full history and you can query that, but equally important is that it
removes all the invalid data. Showing only validated and known to be
correct information to users of its APIs.

### System Requirements

The hub has been tested on Linux systems, but should work on Windows or
Mac, let us know how it works for you so we can update our docs.

The expectation is that Bitcoin Cash will enable a lot more users and thus
a lot more transactions per day as time goes on, as such it is good to
expect growth.

The disk-space usage is minimal for Flowee components itself, but the
blockchain is the one that needs a bit more attention. Please expect at
least 250&nbsp;GB of disk space usage. There is little reason for that to
be fast disk access, so a cheap spin-disk is fine. Suggestion is to
allocate a TB for this.

Memory-wise, a standard 8GB machine should be enough, 16GB is better.

For CPU, we require at least a 64-bit CPU, dual-core, which all modern ones
are. Faster is better, but actual cpu-usage depends completely on your workload
and a little bit on how many people will start using Bitcoin for transactions.

### Configuration

To setup and configure your Flowee infrastructure you want to take a look
at the [plan your deployment](plan-your-deployment) document which will
help you learn about config files, directory structures and more.

### API

The Hub API documentation is for developers writing tools. Please see [API
docs](/docs/api/) for more.
