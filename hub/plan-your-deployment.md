+++
title = "Plan Your Deployment"
toc= true
parent = "hub"
weight = 10
+++

Flowee is a family of products. The main one being Flowee the Hub. Each product has its own set of deployment suggestions, but the important part to remember is that there is no requirement for all of those components to live on the same hardware. With some smart networking, they don't even have to be in the same building.

This page details the deployment structures and ideas for Flowee **the Hub**. Which is the most important component as it lives in the center of your setup.

### Hardware

Flowee the Hub should be able to scale based on the activity of the Bitcoin Cash network. We expect that to go up quite a bit in the next years so please keep that in mind when you decide on hardware.

* CPU  
The Hub requires at least a dual-core machine, 64 bit. More cores is better. We did most of our testing on a quad-core machine which (through hyperthreading) gives 8 parallel queues. This is a comfortable fit at the beginning of 2018.

* Memory  
The Hub works fine on an 8&nbsp;GB machine, but for faster processing 16&nbsp;GB is useful.

* Disk space  
The Hub stores the entire blockchain which is currently around 160&nbsp;GB. We suggest to allocate around 1TB for the Hub.
Please note the part below about [directory structure](#directory-structure-and-user-setup) which explains how this does not have to
be all on one drive.

* Network  
Flowee the Hub requires an internet connection in order to keep up-to-date with the Bitcoin network. this means it should permanently stay connected. The speed of your internet is mostly going to be a question of getting through the initial download of the blockchain. As said that is currently 160&nbsp;GB. Just make sure you don't use the cheapest option.

### Users / services

On Linux the Hub is a so called daemon. A service that runs without a user interface.

### Directory Structure and user setup

We suggest running the Hub under its own user as a system-service.
This has the advantage that the system will auto-start the Hub on boot and restart it should it be killed for some reason.

The most tested setup is this;

Config files in /etc/flowee/
: the [flowee.conf](../configuration/) and [logs.conf](../log-config/) files
  will be read from this if you make your 'flowee' user have its homedir
  there.

Log files in /var/log/flowee
: As this is a unix standard this is our suggestion. Configure this
  yourself in [logs.conf](../log-config/), without any config the logs will go
  to the datadir.

Data dir in /var/lib/flowee
: This is where the blockchain is stored.  
Flowee is rather flexible in those directory locations. The data-directory is where the blockchain goes and that means it will store by far the most actual data. But that doesn't all have to go into the one directory as Flowee is capable of having a series of directories where it will look for block files.  
As such system administrators have the opportunity to attach multiple drives to the system and spread the block data over them, as you see fit. Historical data can even be stored on network drives as the Hub doesn't require the files to be writable. (see `blockdatadir` in [configuration](../configuration))

Cookie files in /etc/flowee/
: Cookie files are auto-generated files used for authentication between the
  Hub and other components. You should store them in a directory only
  readable by authorized operators.  
  The cookies are auto-created the first time the server starts and it
  doesn't find the cookie file on disk.  Restarting the server will not
  change the authentication credentials.


### Spreading Flowee over multiple machines

Flowee the Hub is a completely stand-alone piece of infrastructure. It only
needs internet and it creates its own trust.
If you have a need to access a lot of blockchain data please allow for the
idea where you end up duplicating the Hub to different machines in order
to do load-balancing.

In this case you might want to duplicate the api-cookie to each server to
make authentication easier.

Even with one instance of the Hub, you can still spread your Flowee apps
over different machines. In that case you need to be aware that the
api-server by default only listens on localhost and you need to add
one or more `apilisten` config lines to allow devices on your network
to see it.  
Please be aware that at this time the API communication is not encrypted,
so a setup over multiple machines means anyone with access to your network
can see all the data. We plan to add API encryption in a future
version.


