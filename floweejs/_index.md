+++
date = "2019-12-155T20:00:00+01:00"
title = "FloweeJS"
weight = 20
description = "JavaScript developer resources"
bref = "APIs"
toc = false
singlepage = false
+++

This guide introduces the FloweeJS and discusses some of its fundamental
concepts.

### What is FloweeJS

FloweeJS is a binding module enabling
[JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide)
developers to use the Flowee applications and libraries with the
[NodeJS](https://nodejs.org) JavaScript engine, which itself is based on
the [V8 JavaScript](https://en.wikipedia.org/wiki/V8_%28JavaScript_engine%29)
engine.

Flowee is a family of products and our goal is to move the world towards
a Bitcoin&nbsp;Cash economy. FloweeJS works together with various other Flowee
products for the fastest and simplest way to build Bitcoin&nbsp;Cash products.

The Flowee applications are designed to be cloud-native, which benefits a
FloweeJS developer because it is easy to just use the free services Flowee
provides or, if you want, Flowee services from a 3rd party or even one
maintained by yourself.

Flowee has received [donations](/donations/) that allow us to run a public and free
service for Blockchain data on **api.flowee.org**. The default setup of
FloweeJS will connect to those services.

### Getting FloweeJS

After getting a functional and recent NodeJS installation with NPM, you should
ensure you have the dependencies of the FloweeJS bindings. These are the
[cmake](https://cmake.org) (version 3.13 or later) build tool and the
[boost](https://boost.org) libraries (version 1.56 or later).

After this you can simply run the following in your JS project:
`npm install floweejs`

### Hello World

{{< highlight javascript>}}
var Flowee = require('bindings')('flowee')

// some fun logging callbacks.
Flowee.onConnectHub = function(version) {
    console.log("The Hub reported version: " + version)
}
Flowee.onConnectIndexer = function(services) {
    console.log("Indexer supports services: " + services)
}

// Do the connect
Flowee.connect().then(function() { process.exit(); })
{{< / highlight >}}

### APIs

In most cases you want to start by connecting to the Flowee services.
Details on this can be found in the [connect](connect/) page.

A lot of power is hidden behind the simple [search()](search/) feature.
You will find descriptions of the properties of the
[request](search/#request-object) object and its calbacks, as well as the
properties of the [BlockHeader](search/#blockheader) return type and the
[Transaction](search/#transaction) object in the documentation of the
search() function.

In case you have a system that creates transactions, you will like the
`Flowee.sendTransaction()` function which takes a hex encoded string and
and sends the transaction to your connected hub. A promise is returned.

To monitor an address, for instance for an incoming transaction, you can use
the [AddressMonitor](addressmonitor/) family of functions.

### Basic database structure and design

The Bitcoin&nbsp;Cash coin is a blockchain with millions of transactions.
This is, in effect, a really large database. Some 200GB at the end of 2019.
But it is and will be a database.

To do anything other than a simple interaction with the blockchain you will
need to understand the basic database concepts of Bitcoin&nbsp;Cash in
general, and Flowee in particular.

At the core of this are transactions. Any transaction moves funds unlocked
by `inputs` and stores these funds in `outputs`. With the difference
between them being a miner fee.

Transactions are mined into blocks. A block has a time-stamp, and blocks
are always appended to the end of the chain.

Flowee stores transactions in *the Hub*. The hub uses the number of the
block, also called *blockHeight*, as a *key* for any specific transaction.
The second part of the *key* is *offsetInBlock* which is the byte-position
of the transaction in the block.

The *Indexer* reads the entire blockchain once and creates separate
database tables to allow fast lookup of transactions. For instance there is
the transaction-ID (a 32-byte hash we call TxId) that is unique for each transaction.
Using the Indexer you can resolve that TxId and you will receive the
*(blockHeight,&nbsp;offsetInBlock)* pair that uniquely identifies a transaction in
the Hub.

----

Transactions themselves form a chain (a [Directed acyclic
graph](https://en.wikipedia.org/wiki/Directed_acyclic_graph) (DAG)).
A transaction that spends a previously created transaction-output refers to
it by TxId. From any transaction you can follow the money backwards in time
till the moment the money was created as miner-reward (also called block-reward).

So, the basic transactions already allow going back in time. In order to do
something different we need to be introduced to two new databases.

The UTXO database (short for Unspent-Tx-Outputs) stores all the transaction
outputs that have not yet been spent. Or, in other words, which hold real
money right now. According to the latest validated block, at least.

Any transaction output that has already been spent is removed from the
UTXO. After it's been removed the transaction that spent it is inserted into
the Indexer's *spent* database.

The Spent database allows you to create the reverse DAG, where you can
start at any point from the creation of a coin and follow it forward in
time until the current state.

