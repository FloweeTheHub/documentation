+++
title = "subscribeToAddress()"
weight = 30
toc = true
parent = "floweejs"
+++

This chapter describes the functions and properties related to the address monitor feature.
The address monitor allows JavaScript users to specify a target address and get a notification
when, on the network, something gets deposited on that address.

Where the name `Flowee` is used, this is the variable which results from a call similar to:

`var Flowee = require('bindings')('flowee')`


### Details

The `Flowee.subscribeToAddress()` and `Flowee.unsubscribeToAddress()` are
used to update the global list of addresses that are monitored for us. The
monitoring happens in *the Hub* and when it notices an address being used
it will push a notification to us. The notifications will thus be near
instant as the network notices the new event.

The events coming from the network are sent to the function passed to the
Flowee.onAddressChanged property. The function is called for
**transaction-found**, which is when a transaction is validated and known
to be correct and it stores money in the requested address. On first
subscription of an address, the current mempool will be searched and a
notification will be sent at that time, even if the transaction was sent
earlier.

A second network event that results in the callback is the
**transaction-mined** event. This moves a transaction from being
unconfirmed to be included in a block. Notice that this means a certain
txid can be repoted twice, once when it is found and a second time when it
is mined.

In the case where the transaction is found, but not yet mined, we could see
the sender try to double spend the transaction. Which is an active move
that in most cases is meant to defraud the receiver of the address. General
guidance is that if a double spend is seen within 3 seconds of the initial
*transaction-found* callback, then that transaction should wait for a
confirmation before being accepted as payment.

The Flowee apps detect this all double spends and will call your function
with `reason` of the type **double-spend**. This will include the full 2nd
transaction which double spends the output meant for your registered
address.


### Functions

#### `Flowee.subscribeToAddress()`

###### Parameters

* Address  
The bitcoincash address. We also support legacy address types and
BitcoinScriptHash.

For most usages you can pass in the bitcoincash:address, which supports
both pay-to-public-key-hash (P2PKH) and pay-to-script-hash (P2SH) type of
target addresses.

For more exclusive scripts, and future compatibility, we also allow the
hash of the entire output script to be used. To understand how this works
you should know that on a P2PKH is always the same couple of Bitcoin-Script
operators which are combined with the raw address. Those operators make it
a P2PKH payment. This complete set is stored in the output script.

We can then take the entire output script and make it a constant length by
applying the SHA256 hash algorithm. And this gives us the BitcoinScriptHash
value.

The simple 'bitcoincash' formatted address is accepted as a parameter, but
if you want to accept something more complex this is possible by using the
BitcoinScriptHash.

###### Return-value

This method doesn't have a return value, errors will be thrown as exceptions.

#### `Flowee.unsubscribeToAddress()`

###### Parameters

* Address  
Same as in Flowee.subscribeToAddress()

###### Return-value

This method doesn't have a return value, errors will be thrown as exceptions.


### Callbacks

#### `Flowee.onAddressChanged`

Every time an event happens on the network on one of the addresses
registred, the method registred on this property will be called.

###### Parameters

* changeObject


Example:

{{< highlight js >}}

Flowee.onAddressChanged = function(change) {
    console.log(change);
}

Flowee.connect();
Flowee.subscribeToAddress("bitcoincash:qz50e9ycu4u3rtqman03vzg4ea7mvw5zguj48xf26t");
{{< / highlight >}}

Here an example output for the event 'transaction-found':

{{< highlight json >}}
{
  bitcoinScriptHashed:
'c37e19b20fc9ea0b8110757b81eb9310ba5dd6dc139ae9021b3941b511087816',
  address: 'bitcoincash:qz50e9ycu4u3rtqman03vzg4ea7mvw5zguj48xf26t',
  txid: '6cac683af01a65d1260651fad3d3d827b644cd1c73682f5357fb062bbbd6872e',
  amount: 546,
  reason: 'transaction-found'
}
{{< / highlight >}}

The same transaction was later confirmed which resulted in this
'transaction-mined' event.

{{< highlight json >}}
{
  bitcoinScriptHashed:
'c37e19b20fc9ea0b8110757b81eb9310ba5dd6dc139ae9021b3941b511087816',
  address: 'bitcoincash:qz50e9ycu4u3rtqman03vzg4ea7mvw5zguj48xf26t',
  amount: 546,
  offsetInBlock: 222345,
  reason: 'transaction-mined'
}
{{< / highlight >}}


