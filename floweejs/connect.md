+++
title = "connect()"
weight = 20
toc = true
parent = "floweejs"
+++

This chapter describes the functions and properties related to the connect family of features.

Where the name `Flowee` is used, this is the variable which results from a call similar to:

`var Flowee = require('bindings')('flowee')`


### Functions

#### `Flowee.connect()`

This function starts a (new) connect to a Flowee server-pair. It is
implicitly calling both a *connectHub()* and a *connectIndexer()*.

###### Parameters

* `Hostname` (optional)  
This is the hostname or IP address to connect to. If none is
provided then the connection will be opened to *api.flowee.org*.

###### Return-value

This function returns a promise. Notice that the promise in some cases requires at least NodeJS v12.6

#### `Flowee.connectHub()`

This function starts a (new) connect to Flowee the Hub, which is the main
database for transaction and block-data and is able to receive new
transaction you may have created.

###### Parameters

* `Hostname` (optional)  
This is the hostname or IP address to connect to. If none is
provided then the connection will be opened to *api.flowee.org*.

* `port` (optional)  
The port that the server is listening on. By default this is *1235*.

###### Return-value

This function returns a promise. Notice that the promise in some cases requires at least NodeJS v12.6

#### `Flowee.connectIndexer()`

This function starts a (new) connect to Flowee Indexer, which hosts
one or more indexes which allow fast lookup. Things like finding the
transaction matching a txid, or which transactions deposited funds in a
certain address.

###### Parameters

* `Hostname` (optional)  
This is the hostname or IP address to connect to. If none is
provided then the connection will be opened to *api.flowee.org*.

* `port` (optional)  
The port that the server is listening on. By default this is *1234*.

###### Return-value

This function returns a promise. Notice that the promise in some cases requires at least NodeJS v12.6

### Callbacks

Callbacks are properties you can set a function on, which means they will
be executed when certain situations occur.

#### `Flowee.onConnectHub`

Every single time that a connection is successfully created to Flowee the
Hub the callback assigned to this property will be executed.

###### Parameters

* `server-version`  
The server version is a string much like this one: "*Flowee:1 (2019-a.1)*"

#### `Flowee.onConnectIndexer`

Every single time that a connection is successfully created to Flowee
Indexer the callback assigned to this property will be executed.

###### Parameters

* `services`  
This is an array of strings identifying the indexers that the server we
just connected to provides.

#### `Flowee.onConnected`

When both Flowee the Hub as well as Flowee Indexer have successfully
connected is this callback called.

###### Parameters

*None*
