+++
title = "Service/Blockchain"
toc= true
weight = 1
parent = "api"
+++

### Blockchain Service

The hub provides access to the blockchain, which is easiest to see as a big
database.  The blockchain is a list of blocks and each block has a list of
transactions. Because this database is too big for iteration you need to
understand the way that *Flowee the Hub* wants you to access the data if
you want to get the best speed.

Blocks are sequencial, which means that you can request any block by its
height, a natural number increasing continuesly. If you request data that
is very recent we suggest to use the blockhash instead to avoid reorgs causing
hard to trace bugs.

Inside of blocks we list transacions serially as well, but beware that we
don't use an index on blocks. Instead services use "offset-in-block" as an
ID. This is the amount of bytes into the block that the transaction is
stored. Notice that the first transaction (the coinbase) is always at
position 81.

Inside of the tranaction we don't have any sort of indexing, but
pre-processing the transaction on the Hub will in many cases still be
faster than sending the entire thing to clients. As such we allow a large
range of options to fetch specific pieces of the transaction. See the
GetBlock and GetTransaction messages for details.

This service has Service-Id: `1`

### Datatypes are raw

Transaction-ids, block-ids and addresses are always sent and expected as
raw byte-arrays.

Hashes are often printed in hex form, which is easy to recognize because
they become twice as long. In the API we want the fully decoded values. A
sha256 block-hash is 32 bytes.

Addresses are often encoded using methods like cash-address or base58, this
should be done as close as possible to the user and only fully decoded
versions are accepted on the API. Addresses are 20 byte hashes.

### Messages

#### GetBlockChainInfo

<table class="api">
<tr><td colspan="3">Hub / API. sId=1 mId=<code>0</code></td></tr>
</table>

#### GetBlockChainInfoReply

<table class="api">
<tr><td colspan="3">Hub / API. sId=1 mId=<code>1</code></td></tr>
<tr><td>Difficulty: <code>64</code></td><td>floating-point</td><td>Difficulty of chain</td></tr>
<tr><td>MedianTime: <code>65</code></td><td>natural-number</td><td>time in seconds</td></tr>
<tr><td>ChainWork: <code>66</code></td><td>sha256</td><td>Total amount of work in chain</td></tr>
<tr><td>Chain: <code>67</code></td><td>string</td><td>Chain ID like "main"</td></tr>
<tr><td>Blocks: <code>68</code></td><td>natural-number</td><td>amount of
blocks processed</td></tr>
<tr><td>Headers: <code>69</code></td><td>natural-number</td><td>Number of headers we validated</td></tr>
<tr><td>BestBlockHash: <code>70</code></td><td>sha256</td><td>Chain-tips block-hash</td></tr>
<tr><td>VerificationProgress: <code>71</code></td><td>floating-point</td><td>Number between 0 and 1</td></tr>
</table>

#### GetBestBlockHash

<table class="api">
<tr><td colspan="3">Hub / API. sId=1 mId=<code>2</code></td></tr>
</table>

#### GetBestBlockHashReply

<table class="api">
<tr><td colspan="3">Hub / API. sId=1 mId=<code>3</code></td></tr>
<tr><td>GenericByteArray: <code>1</code></td><td>bytearray</td><td>32-byte hash</td></tr>
</table>

#### GetBlock

This one method (and its sister method [GetTransaction](#gettransaction)) hide a lot of power
because there are a large number of settins to request exactly how to
return a block. You can filter transactions to be included, you can choose
to pre-parse the transaction and return certain parts only.

Notice that GetBlock has a memory between calls of the keys to filter on.
See ReuseAddressFilter / SetFilterAddress / AddFilterAddress.

<table class="api">
<tr><td colspan="3">Hub / API. sId=1 mId=<code>4</code></td></tr>

<tr><td>BlockHash: <code>5</code></td><td>bytearray</td><td>32-byte
blockhash. Alternative to BlockHeight</td></tr>
<tr><td>BlockHeight: <code>7</code></td><td>number</td><td>Height
of the block in the chain.</td></tr>
<tr><td>ReuseScriptHashFilter: <code>40</code></td><td>bool</td><td>Resuse an
address filter (ScriptHash based) created on a previous GetBlock call.</td></tr>
<tr><td>SetFilterScriptHash: <code>41</code></td><td>bytearray</td><td>Clears
and add a filter address</td></tr>
<tr><td>AddFilterScriptHash: <code>42</code></td><td>bytearray</td><td>Add a
20 byte bitcoin address.</td></tr>
<tr><td colspan="3"><b>Select how transactions should be reported in the reply</b>.</td><tr>
<tr><td>Include_TxId: <code>43</code></td><td>bool</td><td>Return a txid
field for selected transactions.</td></tr>
<tr><td>Include_OffsetInBlock: <code>44</code></td><td>bool</td><td>Return
offset in block field for selected transactions.</td></tr>
<tr><td>FullTransactionData: <code>45</code></td><td>bool</td><td>Return
full transaction data. Default true, unless one of the "Include" fields is
true.</td></tr>
<tr><td>Include_Inputs: <code>46</code></td><td>bool</td><td>Return all
inputs for selected transactions</td></tr>
<tr><td>Include_OutputAmounts: <code>47</code></td><td>bool</td><td>Return
amounts (in satoshi) for selected transactions</td></tr>
<tr><td>Include_OutputScripts: <code>48</code></td><td>bool</td><td>Return
out-scripts for selected transactions.</td></tr>
<tr><td>Include_Outputs: <code>49</code></td><td>bool</td><td>Return all
outputs for selected transactions.</td></tr>
<tr><td>Include_OutputAddresses:
<code>50</code></td><td>bool</td><td>Return output-addresses selected transactions, if they are p2pkh or p2pk.</td></tr>
<tr><td>Include_OutputScriptHash: <code>51</code></td><td>bool</td><td>Return output-script hashed. Supports all payment types.</td></tr>
</table>

For all the include\_\* fields the rule is that if they are not included in
the request, they are set to false.

The FullTransactionData field is a bit more complex if not included
explicitly, it is true (send full transaction data) by default, unless the
request selects one or more Include\_\* fields, then it assumes you only
want those and it stops sending the full transaction data.

Please note that there is significant overlap in the options that will
result in the sending of transaction data. The API code will only include a
specific result once, no matter how often it is selected.

#### GetBlockReply

<table class="api">
<tr><td colspan="3">Hub / API. sId=1 mId=<code>5</code></td></tr>
<tr><td>Full raw transaction: <code>1</code></td><td>bytearray</td>A full transaction<td></td></tr>
<tr><td>TxId: <code>4</code></td><td>bytearray</td><td>32-byte transaction hash</td></tr>
<tr><td>BlockHash: <code>5</code></td><td>bytearray</td><td>32-byte blockhash.</td></tr>
<tr><td>BlockHeight: <code>7</code></td><td>natural-number</td><td>Height of the block in the chain.</td></tr>
<tr><td>Tx_OffsetInBlock: <code>8</code></td><td>natural-number</td><td>Key
useful for GetTransaction</td></tr>

<tr><td>Tx_IN_TxId <code>20</code></td><td>bytearray</td><td>32-byte hash
(sha256) of the spending txid that this input spends.</td></tr>
<tr><td>Tx_IN_OutIndex <code>21</code></td><td>natural-number</td><td>index
input in transaction identified Tx_IN_TxID that this input is spending.</td></tr>
<tr><td>Tx_InputScript <code>22</code></td><td>bytearray</td><td>Full
input script. Typically pubkeys and signatures.</td></tr>
<tr><td>Tx_Out_Index <code>24</code></td><td>natural-number</td><td>the
index of the output in the current transaction.</td></tr>
<tr><td>Tx_Out_Amount <code>6</code></td><td>positive-number</td><td>The
amount of satoshis this output holds.</td></tr>
<tr><td>Tx_OutputScript <code>23</code></td><td>bytearray</td><td>Full
outputscript</td></tr>
</table>

Due to filters the answer may not include all transactions in a block and
for transactions included outputs may also be filtered. Outputs that are
included will thus always be prefixed with a Tx_Out_Index field indicating
which index the next data is about.


#### GetBlockVerbose

This is a mapping to the legacy JSON RPC API to return a header of the
block plus a list of transaction-ids.

<table class="api">
<tr><td colspan="3">Hub / API. sId=1 mId=<code>6</code></td></tr>
<tr><td>BlockHash: <code>5</code></td><td>bytearray</td><td>32-byte
blockhash. Alternative to BlockHeight</td></tr>
<tr><td>BlockHeight: <code>7</code></td><td>number</td><td>Height
of the block in the chain.</td></tr>
<tr><td>Verbose: <code>60</code></td><td>bool</td><td>If false, return the
raw block data (default: true)</td></tr>
</table>

#### GetBlockVerboseReply

<table class="api">
<tr><td colspan="3">Hub / API. sId=1 mId=<code>7</code></td></tr>
<tr><td>Time: <code>63</code></td><td>natural-number</td><td>time in seconds</td></tr>
<tr><td>Difficulty: <code>64</code></td><td>floating-point</td><td>Difficulty of chain</td></tr>
<tr><td>MedianTime: <code>65</code></td><td>natural-number</td><td>time in seconds</td></tr>
<tr><td>ChainWork: <code>66</code></td><td>sha256</td><td>Total amount of work in chain</td></tr>

<tr><td>Nonce: <code>74</code></td><td>natural-number</td><td>miner defined
random number</td></tr>
<tr><td>Bits: <code>75</code></td><td>bytearray</td><td>The bits</td></tr>
<tr><td>PrevBlockHash: <code>76</code></td><td>sha256</td><td>previous
block hash (aka blockid)</td></tr>
<tr><td>NextBlockHash: <code>77</code></td><td>sha256</td><td>next
block hash (aka blockid)</td></tr>
</table>

#### GetBlockHeader

<table class="api">
<tr><td colspan="3">Hub / API. sId=1 mId=<code>8</code></td></tr>
</table>

#### GetBlockHeaderReply

<table class="api">
<tr><td colspan="3">Hub / API. sId=1 mId=<code>9</code></td></tr>

<tr><td>BlockHash: <code>5</code></td><td>bytearray</td><td>32-byte blockhash.</td></tr>
<tr><td>Confirmations: <code>72</code></td><td>natural-number</td><td>Amount
of blocks build on top</td></tr>
<tr><td>BlockHeight: <code>7</code></td><td>natural number</td><td>Block index in the chain.</td></tr>
<tr><td>Version: <code>62</code></td><td>natural-number</td><td>block-version</td></tr>
<tr><td>MerkleRoot: <code>73</code></td><td>bytearray</td><td>32 byte sha256, merkleroot</td></tr>
<tr><td>Time: <code>63</code></td><td>natural-number</td><td>time in seconds</td></tr>
<tr><td>Difficulty: <code>64</code></td><td>floating-point</td><td>Difficulty of chain</td></tr>
<tr><td>MedianTime: <code>65</code></td><td>natural-number</td><td>time in seconds</td></tr>

<tr><td>Nonce: <code>74</code></td><td>natural-number</td><td>miner defined
random number</td></tr>
<tr><td>Bits: <code>75</code></td><td>bytearray</td><td>The bits</td></tr>
<tr><td>PrevBlockHash: <code>76</code></td><td>sha256</td><td>previous
block hash (aka blockid)</td></tr>
<tr><td>NextBlockHash: <code>77</code></td><td>sha256</td><td>next
block hash (aka blockid)</td></tr>
</table>

#### GetBlockCount

<table class="api">
<tr><td colspan="3">Hub / API. sId=1 mId=<code>10</code></td></tr>
</table>

#### GetBlockCountReply

<table class="api">
<tr><td colspan="3">Hub / API. sId=1 mId=<code>11</code></td></tr>
<tr><td>BlockHeight: <code>7</code></td><td>natural number</td><td>number
of validated blocks in chain.</td></tr>
</table>

#### GetTransaction

<table class="api">
<tr><td colspan="3">Hub / API. sId=1 mId=<code>12</code></td></tr>

<tr><td>BlockHash: <code>5</code></td><td>bytearray</td><td>32-byte
blockhash. Alternative to BlockHeight</td></tr>
<tr><td>BlockHeight: <code>7</code></td><td>number</td><td>Height
of the block in the chain.</td></tr>
<tr><td>Tx_OffsetInBlock: <code>8</code></td><td>natural-number</td><td>Byte-offst in block</td></tr>
<tr><td>Include_TxId: <code>43</code></td><td>bool</td><td>Return a txid
field for selected transactions.</td></tr>
<tr><td>Include_OffsetInBlock: <code>44</code></td><td>bool</td><td>Return
offset in block field for selected transactions.</td></tr>
<tr><td>FullTransactionData: <code>45</code></td><td>bool</td><td>Return
full transaction data. Default true, unless one of the "Include" fields is
true.</td></tr>
<tr><td>Include_Inputs: <code>46</code></td><td>bool</td><td>Return all
inputs for selected transactions</td></tr>
<tr><td>Include_OutputAmounts: <code>47</code></td><td>bool</td><td>Return
amounts (in satoshi) for selected transactions</td></tr>
<tr><td>Include_OutputScripts: <code>48</code></td><td>bool</td><td>Return
out-scripts for selected transactions.</td></tr>
<tr><td>Include_Outputs: <code>49</code></td><td>bool</td><td>Return all
outputs for selected transactions.</td></tr>
<tr><td>Include_OutputAddresses: <code>50</code></td><td>bool</td>
<td>Return output-addresses selected transactions, if they are p2pkh or p2pk.</td></tr>
<tr><td>Include_OutputScriptHash: <code>51</code></td><td>bool</td><td>Return output-script hashed. Supports all payment types.</td></tr>
<tr><td>FilterOutputIndex: <code>52</code></td><td>number</td>
<td>Only include 'output' items for this specific output-index.</br>
Can be passed multiple times.</br>
Sine 2019.6</td></tr>
</table>


#### GetTransactionReply

<table class="api">
<tr><td colspan="3">Hub / API. sId=1 mId=<code>13</code></td></tr>
<tr><td>Full raw transaction: <code>1</code></td><td>bytearray</td><td>A full transaction</td></tr>
<tr><td>TxId: <code>4</code></td><td>bytearray</td><td>32-byte transaction hash</td></tr>
<tr><td>BlockHeight: <code>7</code></td><td>natural-number</td><td>Height of the block in the chain.</td></tr>
<tr><td>Tx_OffsetInBlock: <code>8</code></td><td>natural-number</td><td>Offset in block</td></tr>
<tr><td>Tx_IN_TxId <code>20</code></td><td>bytearray</td><td>32-byte hash
(sha256) of the spending txid that this input spends.</td></tr>
<tr><td>Tx_IN_OutIndex <code>21</code></td><td>natural-number</td><td>index
input in transaction identified Tx_IN_TxID that this input is spending.</td></tr>
<tr><td>Tx_InputScript <code>22</code></td><td>bytearray</td><td>Full
input script. Typically pubkeys and signatures.</td></tr>
<tr><td>Tx_Out_Index <code>23</code></td><td>natural-number</td><td>the
index of the output in the current transaction.</td></tr>
<tr><td>Tx_OutputScript <code>24</code></td><td>bytearray</td><td>Full
outputscript</td></tr>
</table>

Due to Include\_\* filters the answer may not include all outputs in a
transaction. Outputs that are included will thus always be prefixed with a
Tx_Out_Index field indicating which index the next data is about.

