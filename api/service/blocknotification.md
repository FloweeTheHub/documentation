+++
title = "Service/BlockNotification"
toc= true
weight = 18
parent = "api"
+++

### Block Notification API

This is a subscription style service. Clients can subscribe to receive
block notifications with [NewBlockOnChain](#newblockonchain) messages
whenever a new block is found by the Hub.

The service ID for this service is `18`.

### Messages

#### Subscribe

Subscribing doesn't require any special field, just a simple message.

<table class="api">
<tr><td colspan="3">Hub / BlockNotification. sId=18 mId=<code>0</code></td></tr>
</table>

#### Unsubscribe

Unsubscribing doesn't require any special field, just a simple message.

<table class="api">
<tr><td colspan="3">Hub / BlockNotification. sId=18 mId=<code>2</code></td></tr>
</table>

#### NewBlockOnChain

<table class="api">
<tr><td colspan="3">Hub / BlockNotification. sId=18 mId=<code>4</code></td></tr>
<tr><td>BlockHash: <code>5</code></td><td>bytearray</td><td>32-byte blockhash of the new block.</td></tr>
<tr><td>BlockHeight: <code>7</code></td><td>number</td><td>Height of the block in the chain.</td></tr>
</table>
