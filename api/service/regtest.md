+++
title = "Service/RegTest"
toc= true
weight = 4
parent = "api"
+++

### RegTest service

Bitcoin Cash has 3 typical chains you can work on. There is the main chain
where your coins have real value and experimentation is probably a bad
idea. Then there is the testnet which is over 1.2 million blocks and while
useful in some cases, its usage for developers of applications and sites is
quite limited.

The chain this service deals with is the reg-test chain. The regtest chain
is meant for testing without sharing that chain with others. Although you
can connect up multiple hubs to share the reg-test chain just fine if you
have such needs.

Creating a new block doesn't require a miner or mining hardware, instead a
simple call to [Generate](#generate) creates a new block. As such you can
create your own coins by just generating some blocks.

All transaction creation, fetching and similar features are identical to
the main chain, and as such you can use all other services like normal.

Please note that reg-test mode is a mode in which you start a Hub. A hub
running one chain can not switch to another chain without restart.

The regtest service uses serviceID `4`

### Messages

#### GenerateBlock

Generate block will create a new block with all the transactions from the
mempool (as much as fits in the mining settings anyway).

The block will auto-generate a coinbase transaction which will send the
rewards to the address passed in the request.

<table class="api">
<tr><td colspan="3">Hub / API. sId=4 mId=<code>0</code></td></tr>
<tr><td>BitcoinP2PKHAddress: <code>2</code></td><td>bytearray</td><td>20-byte
ripe160 hash to use as the coinbase output</td></tr>
<tr><td>Amount: <code>6</code></td><td>natural-number</td><td>Amount of
blocks to genrate</td></tr>
</table>

#### GenerateBlockReply

The reply will report which blocks have been mined by listing their sha256
formatted blockId.

Notice that if the request had the amount field and the amount was more
than 1 then you can expect the same number of BlockHash fields in the
answer.

<table class="api">
<tr><td colspan="3">Hub / API. sId=4 mId=<code>1</code></td></tr>
<tr><td>BlockHash: <code>5</code></td><td>bytearray</td><td>32 byte hash of
the block-id mined</td></tr>
</table>

