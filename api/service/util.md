+++
title = "Service/Util"
toc= true
weight = 3
parent = "api"
+++

### Utilities Service

Various utility methods are contained in this service which the Hub is
uniquely qualified to perform. Please note that many standard APIs exist
that do not require communication with the Hub and those will be found in
classes that are provided for your environment instead of in the APIs
described in these documents.

The Utilities service has service-ID `3`

### Datatypes are raw

Transaction-ids, block-ids and addresses are always sent and expected as
raw byte-arrays.

Hashes are often printed in hex form, which is easy to recognize because
they become twice as long. In the API we want the fully decoded values. A
sha256 block-hash is 32 bytes.

Addresses are often encoded using methods like cash-address or base58, this
should be done as close as possible to the user and only fully decoded
versions are accepted on the API. Addresses are 20 byte hashes.

### Messages

#### CreateAddress

Bitcoin addresses come in a combination of a private key, a public key and
we derive from the public key a hash. That hash is what we typically refer
to the actual "address".

Transactions include the address as the place to pay to and in order to
spend it you include the public key and sign the spending transaction with
the private key.

As you can see, you need this data to both receive money and spend it.

The important part is that you need a good amount of randomness in the
creation of the address because others should not be able to predict what
your address is, since they would be able to steal your funds then.

This call allows the Hub to create an address and send it to you. The
default implementation instantly forgets the address and its private key,
but please be sure that you trust the Hub and the operator as well as the
LAN to use this method in a production environment.

<table class="api">
<tr><td colspan="3">Hub / API. sId=3 mId=<code>0</code></td></tr>
</table>

#### CreateAddressReply

<table class="api">
<tr><td colspan="3">Hub / API. sId=3 mId=<code>1</code></td></tr>
<tr><td>BitcoinP2PKHAddress: <code>2</code></td><td>bytearray</td><td>20 byte
bitcoin address</td></tr>
<tr><td>PrivateKey: <code>3</code></td><td>bytearray</td><td>The private
key</td></tr>
</table>

