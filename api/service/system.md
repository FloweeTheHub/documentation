+++
title = "Service/System"
parent = "api"
weight = 126
toc = true
+++

### Header tags

In the network layer we use *messages*, where each message is a single unit
sent from peer to peer. Messages have a maximum length of 9000 bytes and
can be as short as 6 bytes. Message anatomy is further best explained with
an [example](../../example-message/).

Also note the CMF spec for how to encode these on-the-wire. The Network
layer doesn't define the encoding of the body part of messages, the API
will actually use the same CMF encoding, though.

Messages always have a header and optionally have a body. Headers have the
following values reserved;

Header End
: `0`) This tag (typically bool true) denotes the end of the header.

ServiceId
: `1`) This tag is a positive number.  
    We map messages to services by embedding a service-id in the
    message-header. We have several services and users can add their own
    services, as such you can see a service as a routing-id. A message with
    service-id "UtilService" will be sent to the utilties service for
    processing.

MessageID
: `2`) Service-specific message-id.  
Within each service there is a list of messages the service knows how to
handle or create. For instance the System service accepts a Message-id of
type `Ping`. It sends replies of message-type `Pong`.

SequenceStart
: `3`) Marker of sequence start with value of total size.  
The network layer limits message size to no larger than 9000 bytes. To
send a body of larger size you can cut it into separate messages, for
instance of 8 KiB bytes. The sequence of messages starts with this header
tag which contains the total body-size of the big message.

LastInSequence
: `4`) This is a boolean present in any message part of a sequence.  
Each message that is part of a larger sequence (see SequenceStart) should
have this tag and all except the last should have the false value.

Ping
: `5`) Used by the Ping message.

Pong
: `6`) used by the Pong message.

### Sequences

Software layers using the networking layer can need to send data of any
size, for instance large Bitcoin blocks. To avoid any optimizations to
support this on the network layer we introduce message sequences.

A larger message can be split into many small messages, typically each body
measuring only 8KiB in size.

Non sequence messages can be freely mixed into the sequence, they will be
handled as soon as they come in in proper implementations.

A sequence needs to be finished completely before a new one can be started,
violating this rule will typically result in a disconnect and possible ban.

First message;

* ServiceId: positive-number
* MessageId: positive-number
* SequenceStart: positive-number stating the number of bytes in the
  re-essembled body.
* LastInSequence: bool-false
* HeaderEnd: bool-true
* body chunk

Middle-message:

* ServiceId: positive-number
* MessageId: positive-number
* LastInSequence: bool-false
* HeaderEnd: bool-true
* body chunk

Last-message:

* ServiceId: positive-number
* MessageId: positive-number
* LastInSequence: bool-false
* HeaderEnd: bool-false
* body chunk

### Message types

#### Ping

A peer that made a connection to another peer needs to ping every 90-120
seconds, lest they get disconnected.

<table class="api">
<tr><td>ServiceId: 1</td><td>positive-number</td><td>126</td></tr>
<tr><td>Ping: 5</td><td colspan=2>bool-true</td></tr>
<tr><td>Header-end: 0</td><td colspan=2>bool-true</td></tr>
</table>

#### Pong

Pings are always replied to with a pong. A lack of pong for 90-120 seconds
will lead to a disconnect.

<table class="api">
<tr><td>ServiceId: 1</td><td>positive-number</td><td>126</td></tr>
<tr><td>Pong: 6</td><td colspan=2>bool-true</td></tr>
<tr><td>Header-end: 0</td><td colspan=2>bool-true</td></tr>
</table>

