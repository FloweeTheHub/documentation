+++
title = "API Reference documentation"
description = "Flowee services API Reference"
bref = "Flowee Service APIs"
toc = false
singlepage = false
weight = 30
+++

### Overview

All communication between the Hub and other Flowee components happens over
a network-enabled API. This Application Programmer Interface (API) is
designed specifically for the Hub and you should understand several basic
concepts that it is based on in order to get the most out of them.

Using APIs works by connecting to an endpoint of your hub. Connections
happen over TCP/IP bi-directional sockets and the server doesn't normally
disconnect. Which is to say, the API is *not* RESTful. This is a bit
different, but definitely worth it.

API requests are encapsulated in individual messages. One message for a
request, and typically one message for a reply.

A message is comprised of a list of tagged-fields. Each tagged-field is a
triplet of name, format and value. ([specification](protocol-spec/)).

Each message will have a `message-id` as well as a `service-id`. Those two
make up the structure of the API.  You can imagine a list of methods to
call is mapped to a list of message-ids. A unique service allows for a list
of methods, much like methods on a class.

[Example message](example-message/).

### Message-headers

In the protocol I separte the message into a header and a body. The header
has basic information that is used by the network-layer with details like
the `message id` and the `service id`.

A full table is detailed in the [SystemService](service/system) page.

### Services

Each and every message has a `service-id` which maps to a unique service
provided by the server. Clients send a request message and receive an
answer with the same service-id.

For more details, look at the independent services.


[API Service](service/hub/)
: `0`) A meta representing the Hubs API. If any command fails, you'll
get a message from this service.

[BlockChain Service](service/blockchain/)
: `1`) Query the blockchain-database.

[live Transaction Service](service/livetx/)
: `2`) Retrieve or send live transaction data.

[Util Service](service/util/)
: `3`) Generic utilities provided by the hub. For instance 'CreateAddress'

[Test Service](service/regtest/)
: `4`) Local testing service. This is for a Hub running on a testing chain
that is not typically shared with others and as such is possible to stay
completely local and mining is always instant.

[AddressMonitor Service](service/addressmonitor/)
: `17`) Remote can subscribe to bitcoin-address usage notifications

[BlockNotification Service](service/blocknotification/)
: `18`) Subscription service for incoming blocks.

[Indexer  Service](service/indexer/)
: `19`) Service supplied by the [Indexer](/indexer) product, gives access to lookup tables for blockchain data.


[System Service](service/system/)
: `126`) Low level messages that are needed for the network connection and
routing.
