+++
title = "txVulcano tutorial"
weight = 35
description = "Setting up your txVulcano"
bref= ""
singlepage = true
+++

TxVulcano is an application that uses *the Hub* as a place to send transactions to and also to generate blocks so it can use the mining reward as money it sends around with new transactions.

As a result the first thing a new installation of txVulcano will do is create a series of blocks and then it will create transactions spending that.

We start by downloading the zip file with the software from gitlab;

`https://gitlab.com/FloweeTheHub/thehub/pipelines?scope=branches&page=1`

Pick the latest release, for instance 2019.07, and download the "Flowee-linux-zip-artifacts" using the download menu on the right of each row.

Unpack the zip file and you'll notice a new directory created in the format like this: `flowee-2019.07-83d455a5`.  The numbers are a version number. The same version will be visible in the output of the `--version` argument of the executables it contains.

In the bin dir you will find the `hub` and the `txVulcano` executables. Those will be all we use in this tutorial.

Each of the two applications needs to be running at the same time. You probably want to start two terminals (konsole or gnome-terminal) to run them side by side.

Start the hub using:

```
bin/hub --regtest
```

And then while the hub is running we can start the txVulcano in the second terminal. But before we do so, lets take a quick look at the options it provides.

```
bin/txVulcano --help

Usage: ./txVulcano [options]
Transaction generator of epic proportions

Options:
  -h, --help                       Displays this help.
  --block-size, -b <size>          sets a goal to the blocks-size created
  --num-transactions, -n <amount>  Limits number of transactions created
                                   (default=5000000)
  --version                        Display version
  --connect <Hostname>             Server location and port
  --verbose, -v                    Be more verbose
  --quiet, -q                      Be quiet, only errors are shown
```

The default amount of transactions is 5 million. We could change this amount for our tests downwards a bit. Lets start with `-n 100000` and run it;

```
bin/txVulcano -v -n 100000
```

We will immediately see a list of statements like; "Hub mined or found a new block:" with a similar line in the terminal of *the Hub*.

The txVulcano will take around 10 seconds to start and then will continue writing lines like: `Block still 19 MB from goal`.
After a little while it will print `Block is full enough, calling generate()` and `Generate returns with a block hash:  19779af834176cbc6021612992fbd9b8f05a81472c3611c034d0f06f9a098ff5`

After filling one block the entire process of filling a block starts over, keeping you updated of progress. The vulcano stops when it has created the wanted number of transactions.


After our quick test finishes lets make some really big blocks.

```
bin/txVulcano -v --block-size 250 --num-transactions 100000000
```

txVulcano will not immediately go for the 250 MB block, as this is often not what you want, it will create a series of blocks of growing size until it reaches 250MB.

You can interrupt the txVulcano at any time (just hit Ctrl-C) if you feel there have been enough blocks. Or let it run to create a couple of gigabytes of block data.


Have fun!

