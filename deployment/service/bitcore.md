+++
title = "Service/BitCore-proxy"
parent = "deployment"
weight = 30
toc = false
bref = "JSON docs about blockchain data"
+++

This doc is about the Docker container named "flowee/bitcore-proxy".

The Bitcore-proxy container requires both a *flowee/hub* and a *flowee/indexer*
to connect to for its backing data store. The advantage is that containers
for bitcore-proxy stay simple and small. No need for any volumes (storage).

As detailed in the [overview](../../), it is known to work that
you port-forward the API on services like *the Hub* and *Indexer* for
others to connect to it by nothing but the hosts network address. Either a
DNS entry, or a direct IP address. Use this in the *FLOWEE_HUB* env var
below.

A simple setup example:

    docker run -e FLOWEE_HUB=192.168.1.2 -d -p 3000:3000 \
            -e FLOWEE_INDEXER=192.168.1.2 flowee/bitcore-proxy

### Available options (env-vars)

* `FLOWEE_INDEXER`  
  *The hostname or IP and optionally port to connect to the Indexer
  (see [Indexer](../indexer/) container)*

* `FLOWEE_HUB`  
  *The hostname or IP and optionally port to connect to the Hub
  (see [the Hub](../hub/) container)*

* `FLOWEE_LOGLEVEL`  
  *Allows you to change the log-level. Recognized options are `info`, `quiet` or `silent`.*

* `FLOWEE_BITCORE_JSON_VERBOSE`  
  *If this env variable is present (any value) the generated JSON will have pretty indenting and linefeeds.*

