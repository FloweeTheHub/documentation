+++
title = "Getting Started"
date = "2019-04-12T13:00:00+01:00"
weight = 1
description = "Understanding Flowee and getting it set up"
bref= "Intro to general Flowee concepts and how to get the most out of it"
toc= true
singlepage = true
+++

### Understanding Flowee

Flowee is a family of products and our goal is to move the world towards
a Bitcoin&nbsp;Cash economy.

The rise of popularity in cryto-currencies is because it is inherently a
better solution for money than what we have used for the past couple of
centuries. More and more people start using Bitcoin&nbsp;Cash as a means of
exchange and a way to pay for things and services. Bitcoin&nbsp;Cash can not be
censored, confiscated, is instant and is practically free to transact.

What Bitcoin&nbsp;Cash gives is the power for people can take control of
their own money back from their governments.

This freedom gained has as a natural consequence some responsibilities. The
main thing is that we, the people, become the ones responsible for the
infrastructure. The highways over which our money new travels. These roads
are built on the Internet and thus there is not much that the common user
of this new money needs to do.

Flowee focuses on the parties that want to do more with money. From
webstores to salary processing, a large section of our marketplace will
benefit from having something heavier than a wallet on their mobile phone.

This is where *Flowee* products come in. Our applications and services
provide the more heavy users of our future money to verify correctness, to
find historical records and naturally to create and send transactions.

### The Hub

Flowee the Hub is a central product that connects to the Internet and
actively filters the correct from the incorrect. The real money from the
fake and the real customers from the attackers.

Where Bitcoin Cash is trustless, you need Flowee the Hub to do the full set
of validations and verification.

The Hub provides a full record of all transactions made on Bitcoin&nbsp;Cash.
Fully validated and actively kept up to date. It will validate and forward
any transactions you create and want to send to the Bitcoin network. And
last it will provide you with instant updates of other people sending
transactions to you via the Bitcoin&nbsp;Cash network.

### Indexer

*[The Hub](/hub/)* has the full historical history of payments and you can
find all the details of each transaction using it. This concerns a lot of
data. Around 280 million transactions at the time of writing this. An
on-disk usage of around 160GB. To allow any sort of historical
investigation you will want to use a *Flowee Indexer* service.
The indexer is a stand-alone service which makes deployment both
flexible and fast.

This allows you to "follow the money", as it were. Any department that
wants to do any sort of client followup towards payments will want to have
access to an Indexer. Developers doing anything of relevance with Bitcoin
Cash will definitely want to access this data as well.

There are a set of websites that provide similar data to the public, you
can trust those to never lie to you and never be offline. And if your
business is small enough that may be fine. For everyone else there is the
*Indexer*.

### FloweeJS

This product allows usage of the immensely popular Javascript programming
language to build applications that use the Bitcoin&nbsp;Cash blockchain.



FloweeJS is the bridge between the 

*[FloweeJS](/floweejs/)* works closely with the hugely popular
[NodeJS](https://nodejs.org) Javascript engine. People that want to tie in
their own products into Flowee's products can use the Javascript
programming language.

You can write your applications fully in JavaScript and access the Hub and
the Indexer databases in an easy to use manner. For simple lookups on
address, transaction-id and more.

You can run this on your own server and use the public and free APIs, and
when your usage grows larger you can run your own Flowee the Hub to provide
your app.

### Bitcore-proxy

One of the earlier web-apis that became often used is the bitcore one.
People that depend on this API may find the bitcore-proxy useful to use
as it uses the Flowee products to find its data. Dropping the need to keep
maintaining and running the very data+cpu expensive bitcore.

If you never used bitcore before, you will likely not find this component
very interesting.

### Deployment: Flowee is Cloud-Native

Flowee ships multiple products. Those products
work together and communicate with each other over standard Internet
connections (sockets) which makes deployment extremely flexible.

As your needs grow, you can just spin up another copy of your backing
services! Or if you want, you can move the indexer to another machine.
Again, with practically no downtime.

This is what it means to be cloud-native. And Flowee is going to make
you smile when you find out how easy this makes your life.

![Flowee Block Diagram](/img/blocks.png)

Please check the [dedicated docs](/docs/deployment/) for how to run and
deploy Flowee applications.

### Running all services as your test-user

The first setup you will most likely use is to download (or compile) the
components and run them on your own machine.

All configurations are stored in $HOME/.config/flowee/  
and all data files by default go to $HOME/.local/share/flowee

Notice that we have a [systemd](https://en.wikipedia.org/wiki/Systemd) file
for *[the
Hub](https://gitlab.com/FloweeTheHub/thehub/blob/master/support/thehub.service)*
and for
*[Indexer](https://gitlab.com/FloweeTheHub/thehub/blob/master/support/indexer.service)*.

