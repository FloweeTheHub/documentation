+++
title = "Double Spend Proofs"
date = "2020-1-12-T13:00:00+01:00"
weight = 100
description = "What they are and how it works"
toc= true
singlepage = true
+++

### Introduction

In my youth I read a the wonderful story about a car manufacturer that made
a very fancy car which had superb traction and when you went through a
corner you would not feel any vibration or other problems while steering it
through even the most sharp corners.

It was amazing. And it was deadly.

When the car got tested by people that were no professional drivers, those
people got confused with the total lack of vibrations, slippage and other
things that happen when you go through a corner too fast. They just kept on
going faster until the car eventually could not compensate, slipped and
crashed.

The car maker ended up artificially added vibrations to the steering wheel
when the car noticed it was getting close to its limits. This made the
people realise they were getting to the limits and they slowed down.

No system is perfect and the average human operator will use it within
limits. The problem with the car was that it didn't make clear what its
limits were.
A system that is hiding all its inner working may forget that showing
weakness and limits are features that we, humans, require in order to
understand how to use it. Having added vibrations to the steering column
made the car a very popular type which is still sold to this day.

Bitcoin Cash is a similar system that hides most of its inner workings.
In Bitcoin Cash the Double Spend Proofs (DSPs) create notifications when
bad things happen. So we don't have an illusion of the system being
perfect. And by getting accurate feedback of where the limits are, we will
be more comfortable in using the system to its fullest.

Double Spend Proofs identify people and systems that are faulty or
intentionally trying to break the system for their own gain.

This identification of such bad actors comes with a cryptographic proof of
the deed and allows users to take action to avoid being stolen from.

### Zero-conf and double spends

When in 2009 Satoshi rolled out the Bitcoin system he solved, for the first
time in history, the decentralised avoidance of double spends in a chain of
transactions.

We have since then started to explore the safety of transactions that are
not yet part of the chain. The so called "zero confirmations" transactions.

We can say that the risk of accepting a payment is directly related to how
many confirmations it has. Where 1 confirmation is when it first gets
mined in a block.
As such the greatest risk is when it has not yet appeared in a block.

Before DSPs the risk for the thief to try and defraud a merchant was zero.
The merchant would not find out and not be notified in all the cases where
the attempt to defraud failed. Allowing the thief to keep on trying with
zero risk.

Empirical research has been done on the current network. People tried
different ways to double spend and realised that the risk of a double spend
is actually quite low. They concluded its not easy to pull a successful
double spend which defrauds a merchant.

What this research also concluded is that the mono-culture of the network
is very important in keeping the double spends from being accepted. All
miners run with practically the same settings which makes timing the main
tool for the attacker to defraud a merchant.

As the network grows it will become inevitable that miners will change
their miner settings in order to become more profitable with their specific
setup. The loss of a mono-culture in Bitcoin Cash mining is inevitable and
likely healthy. The side-effect is that it will become easier to work
around the "first seen" principle that is our main defence against double
spends. And this in turn means that the double spend proof will end up
being a increasingly valuable tool in the toolbox to keep the network
healthy and the instant payment feature low risk.

### Two conflicting transactions, how does that work?

The basic Bitcoin Cash transaction takes an existing "output" and spends
it. The value embedded in that output we are spending is then send
elsewhere in our transaction, itself in turn creating a new output.

This triple entry accounting where your transaction removes one output and
creates a new one assumes that there is only one entry that spends any
single output. Should there be two independent transactions which both
spend the same output and send it to different places, that is what we call
a double spend.

Because in Bitcoin Cash all transactions are cryptographically sealed, the
attempt to move one output twice is a standalone proof that the owner is
responsible for those two transactions. This follows from the simple fact
that the owner of the output is the only one that can move the funds.

A double spend proof is not much more than a pointer to the output it tries
to double spend and essential parts of the transactions doing the spending.
Including both cryptographic signatures, of both spends, that only the
owner could have made.

It is then just a matter of validating both signatures that allows us to
establish without a doubt that the output has been spent twice.

The full specification of the DSP is [here](/docs/dsp-spec/).
