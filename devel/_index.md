+++
date = "2019-04-13T12:00:00+01:00"
title = "Developers notes"
draft = false
weight = 80
description = "Flowee the Hub developers reference notes"
bref = "C++ Developers information"
toc = false
singlepage = false
+++

Please take a look at the topics if anything is useful for you.

If you are not actually working ON Flowee, you may be searching for the API docs;

The Hub API documentation is for developers writing tools. Please see [API docs](/docs/api/) for more.


### Introduction

The Hub is not a new codebase, it in fact is the same codebase that Satoshi
originally wrote for Bitcoin in 2008-2011. Back then it was written to be
everything in one product. Flowee realized that a product that tries to
do everything will end up not being very good at anything.

The hub is what is left, we kept all the validation, the mempool and
deprecated the creation of transactions (the wallet) and other parts
which make little sense for most users.

As a result of deprecating things like creation of data we now can fully
leverage the benefits of blockchain technology. It is read-only by
definition.

We added [memory-mapped blocks](memmapped-blocks), which drastically
reduces memory usage and loading times. This then lead to the ability to
make Flowee massively parallel enabled. If you have a machine with multiple
cores, we actually use them. Details are in the
[multi-threaded](multithreaded) design documents. This is used for the
validation engine, as well as for the [network manager](network-manager).

Less exciting but extremely useful is that the [logging
framework](../hub/log-config) has been greatly upgraded, compared to the
rather weak and single-minded version that the competition uses there is
actually a rather large advantage there.

Operators and developers will want to read the [logging levels
guidelines](loglevel-guidelines) to get the best out of the system.

