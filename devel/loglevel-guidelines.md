+++
title = "Log-Level Guidelines"
#toc= true
bref = "For operators and developers"
parent = "devel"
weight = 400
+++

### Log-Level Guidelines

In Flowee we have a logging framework which is useful for multiple purposes
from developer (debugging) information to fault-finding and general
operational monitoring.

**Log Levels**

The software developer writes code to log events and he has to choose a log-level. Similarly, the operator will choose a log level of which events to actually process.

Software with this line `logCritical(Log::RPC) << "event";` will only log
if the operator had selected an appropriate log-level for the RPC
log-section.

| Level | Usage | To show them, use keyword |
|---|----|---|
|debug| Removed from release builds, only for developers | debug |
|info| Generic non-unique event, high-quantity expected| info |
|warning| Similar as info, but for things that went wrong| info |
|critical| High-level events, don't happen often. Great for understanding what is going on| quiet |
|fatal| Very rare events, can not be silenced | silent |

The guideline for programmers to choose a log level is based on who for and why it is logged. More specific examples:

* Info/warning are events that are for profiling or debugging a node by an operator. So you'll expect one for each network message, for instance.
* Critical messages are for operators that want to know more about the state of the node or the network. Compare this to a dashboard in a car. A small subsection of useful information is shown here.
* Fatal is typically only used when a component is about to shutdown.

See also [logging Sections](/docs/hub/log-sections/) & [Configuration](../../hub/log-config/).
